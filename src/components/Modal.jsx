import React from "react";
import { ProductContextValue } from "../context";
import { Link } from "react-router-dom";

const Modal = () => {
  const { showModal, modalProduct, setShowModal } = ProductContextValue();

  if (showModal) {
    const { img, title, price } = modalProduct;
    return (
      <div className="modal-container">
        <div className="container">
          <div className="row">
            <div className="col-8 mx-auto col-md-6 col-lg-4 text-center text-capitalize p-5">
              <h5>item added to cart</h5>
              <img src={img} alt="product" className="img-fluid" />
              <h5>{title}</h5>
              <h5 className="text-muted">price: $ {price}</h5>
              <Link to="/">
                <button
                  onClick={() => setShowModal(false)}
                  className="cart-button ml-1"
                >
                  continue shopping
                </button>
              </Link>
              <Link to="/cart">
                <button
                  onClick={() => setShowModal(false)}
                  className="cart-button-yellow ml-1"
                >
                  go to cart
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return null;
};

export default Modal;
