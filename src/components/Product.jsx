import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { ProductContextValue } from "../context";

const Product = props => {
  const {
    addToCart,
    showDetail,
    checkProductInCart,
    setShowModal
  } = ProductContextValue();
  const { product } = props;
  const { title, img, price, id } = product;
  const inCart = checkProductInCart(id);
  return (
    <div className="product-container col-9 mx-auto col-md-6 col-lg-3 my-3">
      <div className="card">
        <div className="img-container p-5" onClick={() => showDetail(id)}>
          <Link to="/details">
            <img src={img} alt="product" className="card-img-top" />
          </Link>

          <button
            className="product-cart-btn"
            disabled={inCart}
            onClick={() => {
              addToCart(id);
              setShowModal(true);
            }}
          >
            {inCart ? (
              <p className="text-capitalize mb-0" disabled>
                {""}in cart
              </p>
            ) : (
              <i className="fas fa-cart-plus" />
            )}
          </button>
        </div>
        {/* card footer */}
        <div className="card-footer d-flex justify-content-between">
          <p className="align-self-center mb-0">{title}</p>
          <h5 className="text-blue font-italic mb-0">
            <span-mr-1>${price}</span-mr-1>
          </h5>
        </div>
      </div>
    </div>
  );
};

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    inCart: PropTypes.bool
  }).isRequired
};

export default Product;
