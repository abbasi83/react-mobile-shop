import React from "react";

import { ProductContextValue } from "../../context";
import PayPalButton from "./PayPalButton";

const CartTotals = props => {
  const { cartFuncs, cartTotals } = ProductContextValue();

  return (
    <div className="container">
      <div className="row">
        <div className="col-10 mt-2 ml-sm-5 ml-md-auto col-sm-8 text-capitalize text-right">
          <button
            type="button"
            className="btn btn-outline-danger text-uppercase mb-3 px-5"
            onClick={() => cartFuncs.clearCart()}
          >
            clear cart
          </button>

          <h5>
            <span className="text-title">subtotal :</span>
            <strong>$ {cartTotals.subtotal}</strong>
          </h5>

          <h5>
            <span className="text-title">tax :</span>
            <strong>$ {cartTotals.tax}</strong>
          </h5>
          <h5>
            <span className="text-title">total :</span>
            <strong>$ {cartTotals.total}</strong>
          </h5>
          <PayPalButton
            total={cartTotals.total}
            clearCart={cartFuncs.clearCart}
            history={props.history}
          />
        </div>
      </div>
    </div>
  );
};

export default CartTotals;
