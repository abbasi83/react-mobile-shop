import React from "react";
import Title from "../Title";
import CartColumns from "./CartColumns";
import EmptyCart from "./EmptyCart";
import { ProductContextValue } from "../../context";
import CartList from "./CartList";

const Cart = props => {
  const { cart } = ProductContextValue();
  const isEmpty = cart.length === 0;

  return (
    <React.Fragment>
      {!isEmpty && (
        <section>
          <Title name="your" title="cart" />
          <CartColumns />
          <CartList {...props} />
        </section>
      )}
      {isEmpty && <EmptyCart />}
    </React.Fragment>
  );
};

export default Cart;
