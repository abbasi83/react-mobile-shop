import React from "react";
import CartItem from "./CartItem";
import { ProductContextValue } from "../../context";
import CartTotals from "./CartTotals";

const CartList = props => {
  const { cart } = ProductContextValue();
  return (
    <div className="container-fluid">
      {cart.map(item => {
        return <CartItem key={item.id} item={item} />;
      })}
      <CartTotals {...props} />
    </div>
  );
};

export default CartList;
