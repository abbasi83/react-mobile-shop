import React from "react";
import { ProductContextValue } from "../../context";

const CartItem = props => {
  const { item } = props;
  const { id, title, img, price, count } = item;
  const { cartFuncs } = ProductContextValue();
  return (
    <div className="row mt-2 mb-5 text-capitalize text-center">
      <div className="col-10 mx-auto col-lg-2">
        <img
          src={img}
          alt="product"
          style={{ width: "5rem", height: "5rem" }}
          className="img-fluid"
        />
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <span className="d-lg-none">product:</span>
        {title}
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <span className="d-lg-none">price:</span>
        {price}
      </div>
      <div className="col-10 mx-auto col-lg-2 my-2 my-lg-0">
        <div className="d-flex justify-content-center">
          <div>
            <span
              className="btn btn-black mx-1"
              onClick={() => cartFuncs.decrement(id)}
            >
              -
            </span>
            <span className="btn btn-black mx-1">{count}</span>
            <span
              className="btn btn-black mx-1"
              onClick={() => cartFuncs.increment(id)}
            >
              +
            </span>
          </div>
        </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <div className="cart-icon">
          <i
            className="fas fa-trash"
            onClick={() => cartFuncs.removeItem(id)}
          />
        </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <strong>item total : </strong>
        {price * count}
      </div>
    </div>
  );
};

export default CartItem;
