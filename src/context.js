import React, { useContext, useEffect, useState } from "react";
import { storeProducts, detailProduct } from "./data";

const ProductContext = React.createContext();

const ProductProvider = props => {
  const [products] = useState(storeProducts);
  const [productDetail, setProductDetail] = useState(detailProduct);
  const [cart, setCart] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [modalProduct, setModalProduct] = useState(null);
  const [cartTotals, setCartTotal] = useState({
    subtotal: 20,
    tax: 10,
    total: 30
  });
  const clearCart = () => {
    setCart([]);
  };
  const isProductInCart = id => {
    return cart.some(item => item.id === id);
  };

  const increment = id => {
    if (isProductInCart(id)) {
      const mapped = cart.map(item => {
        if (item.id === id) {
          item.count = item.count + 1;
        }
        return item;
      });
      setCart(mapped);
    }
  };

  const decrement = id => {
    if (isProductInCart(id)) {
      const mapped = cart.map(item => {
        if (item.id === id && item.count > 1) {
          item.count = item.count - 1;
        }
        return item;
      });
      setCart(mapped);
    }
  };

  const addToCart = id => {
    if (!isProductInCart(id)) {
      const product = getItem(id);
      const item = { ...product };
      item.count = 1;
      setCart([...cart, item]);
      setModalProduct(item);
    }
  };

  const removeItem = id => {
    if (isProductInCart(id)) {
      const filtered = cart.filter(item => item.id !== id);
      setCart(filtered);
    }
  };

  const cartFuncs = {
    clearCart,
    isProductInCart,
    addToCart,
    removeItem,
    increment,
    decrement
  };

  useEffect(() => {
    const subtotal = cart.reduce((total, item) => {
      return total + item.price * item.count;
    }, 0);

    const tax = parseFloat((subtotal * 0.1).toFixed(2));
    const total = subtotal + tax;
    setCartTotal({ subtotal, tax, total });
  }, [cart]);

  const getItem = id => {
    return products.find(item => item.id === id);
  };
  const showDetail = id => {
    const product = getItem(id);
    setProductDetail(product);
  };

  const state = {
    products,
    productDetail,
    addToCart,
    showDetail,
    checkProductInCart: isProductInCart,
    showModal,
    setShowModal,
    modalProduct,
    setModalProduct,
    cartFuncs,
    cart,
    cartTotals
  };

  return (
    <ProductContext.Provider value={state}>
      {props.children}
    </ProductContext.Provider>
  );
};

/*

class ProductProvider extends Component {
  state = { products: storeProducts };
  render() {
    return (
      <ProductContext.Provider value={this.state}>
        {this.props.children}
      </ProductContext.Provider>
    );
  }
}
*/

const ProductContextValue = () => {
  const value = useContext(ProductContext);
  return value;
};

export { ProductProvider, ProductContextValue };
